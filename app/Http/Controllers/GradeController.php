<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GradeController extends Controller
{
    public function index(){
        return view("grade.index");
    }

    public function submitGrade(Request $r){
        $gradeQuis = $r->nQuis;
        $gradeTugas = $r->nTugas;
        $gradeAbsensi = $r->nAbsensi;
        $gradePraktek = $r->nPraktek;
        $gradeUAS = $r->nUAS;

        $sumGrade = $gradeQuis + $gradeTugas + $gradeAbsensi + $gradePraktek + $gradeUAS;
        $aveGrade = $sumGrade / 5;

        if($aveGrade == 100){
            $grade = 'A';
        }elseif($aveGrade < 100 && $aveGrade >= 85){
            $grade = 'B';
        }elseif($aveGrade < 85 && $aveGrade >= 75){
            $grade = 'C';
        }elseif($aveGrade < 75 && $aveGrade >= 65){
            $grade = 'D';
        }elseif($aveGrade < 65){
            $grade = 'E';
        }else{
            $grade = 'Unknown';
        }
        
        return view('grade.result', compact([
            'sumGrade', 
            'aveGrade', 
            'grade', 
            'gradeQuis',
            'gradeTugas',
            'gradeAbsensi',
            'gradePraktek',
            'gradeUAS'
        ]));
    }
}