@extends('layout.main')

@section('content')
<div class="col-4 px-4">
    <form action="/submitGrade" method="POST">
        @csrf
        <div class="mb-3">
            <label for="nQuis" class="form-label">Nilai Quis</label>
            <input type="text" class="form-control" id="nQuis" name="nQuis" placeholder="">
        </div>
        <div class="mb-3">
            <label for="nTugas" class="form-label">Nilai Tugas</label>
            <input type="text" class="form-control" id="nTugas" name="nTugas" placeholder="">
        </div>
        <div class="mb-3">
            <label for="nAbsensi" class="form-label">Nilai Absensi</label>
            <input type="text" class="form-control" id="nAbsensi" name="nAbsensi" placeholder="">
        </div>
        <div class="mb-3">
            <label for="nPraktek" class="form-label">Nilai Praktek</label>
            <input type="text" class="form-control" id="nPraktek" name="nPraktek" placeholder="">
        </div>
        <div class="mb-3">
            <label for="nUAS" class="form-label">Nilai UAS</label>
            <input type="text" class="form-control" id="nUAS" name="nUAS" placeholder="">
        </div>
        <input type="submit" class="btn btn-success" placeholder="Submit">
        <input type="reset" class="btn btn-danger" placeholder="Reset">
    </form>
</div>


@endsection
